Indiegogo working with LibreJS
=============================

This project's goal is to make indiegogo usable when the LibreJS plugin is installed.

Users should be able to install LibreJS, and this plugin, visit indiegogo.com and then
search, browse and contribute to campaigns via the for-profit credit card partner (adyen).

Copyright (C) 2014 Ryan J. F. White
Entire project released under GNU GPL v3 or later
