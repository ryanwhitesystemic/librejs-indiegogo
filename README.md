indiegogo use and credit card payments
======================================
2015-04-02:
Initial xpi file ready for use. Let's see if it's usable by regular folks :)

2015-03-30:
Smoothing rough edges: xpi should be ready for internal team testing now.

2015-03-27:
After a rough patch in which the published xpi wasn't performing the same as the dev code, the published xpi can now also perform deductions. Still very dev-friendly - expecting to polish in the coming weeks. Provided nothing else breaks...

2015-03-12:
LibreStripe functionality now incorporated and working. I've donated a dollar to a project :) There is significant polishing to be done, but it works again. rw

2015-03-10:
Started bringing in LibreStripe functionality - LibreStripe is a drop in replacement for Stripe's own supplied JS. See https://bitbucket.org/ryanwhitesystemic/librestripe for LibreStripe details.

2015-02-04:
Able to successfully process a payment :) but SEARCH has changed - so is not visible from the current edit :(. I've been using https://www.indiegogo.com/projects/dolfi-next-gen-washing-device as a starting point for now.

2014-12-22:
Still working on stripe payment - getting a tok_xxxxxx type token, for performing a payment. Look at igg.site.common for details (line 22685 would get you started). Right now, it's just creating some data - the actual ajax submission is not enabled right now.

2014-11-28:
Current status: broken - there's a failure in calling "sjsonpNNNNNNNN" in the copied and included stripe code - so trying to get to the bottom of that. Once that's solved, reverse engineering can start.

2014-11-06:

Current status: broken - credit card submission form has changed recently - looks like they were using adyen - now they're using stripe... or... they're switching between the two...

It is intended that this will be an add-on for Firefox type browsers that will enable the use of indiegogo.com for searching and contributing, when the LibreJS add-on has been installed.

Copyright (C) 2014 Ryan J. F. White
Entire project released under GNU GPL v3 or later