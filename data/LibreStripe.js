/** @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 *   Copyright (C) 2015 Ryan J. F. White
 *
 *   The JavaScript code in this page is free software: you can
 *   redistribute it and/or modify it under the terms of the GNU
 *   General Public License (GNU GPL) as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option)
 *   any later version.  The code is distributed WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 *   As additional permission under GNU GPL version 3 section 7, you
 *   may distribute non-source (e.g., minimized or compacted) forms of
 *   that code without the copy of the GNU GPL normally required by
 *   section 4, provided you include this license notice and a URL
 *   through which recipients can access the Corresponding Source.
 *
 *  @licend  The above is the entire license notice
 *  for the JavaScript code in this page.
 */
/*
  This is intended as a LibreJS compliant drop in replacement for Stripe's code available here: https://js.stripe.com/v2/
*/
(function(){
    //alert("starting stripe");
    this.Stripe = {
        key:null,
        card:{},
        api_path:"https://api.stripe.com/v1",
        payment_user_agent:"LibreStripe.js/288dcdf",
    };
    this.Stripe.utils = {};
    this.Stripe.utils.underscores = function(txt){
        var processed = txt+"".toLowerCase();
        processed = processed.replace(/-/g,"_");
        return processed;
    }
    this.Stripe.setPublishableKey = function(key){
        //alert("setting key"+key);
        this.key=key;
    };
    this.Stripe.card.createToken = function(form,handler){
        //the data to be sent to the token getting endpoint
        var token_data = {
            key:Stripe.key,
            payment_user_agent:Stripe.payment_user_agent,
            card:{}
        };
        //here, we munge the form for the card data we're after
        $("[data-stripe]").each(function(item){
            token_data.card[Stripe.utils.underscores($(this).attr("data-stripe"))] = $(this).val();
        });
        $.ajax({
            type:"POST",
            url:Stripe.api_path+"/tokens",
            data: token_data,
            success: function(msg,textStatus,jqXHR) {
                handler(textStatus,msg);
                return false;
            },
            error: function(jqXHR,textStatus,errorThrown) {
                var response = JSON.parse(jqXHR.responseText);
                handler(textStatus,response);
                return false;
            },
            complete: function(jqXHR,textStatus){
                //for now, nothing here
                return false;
            }
        });
        return false;
    }
}).call(this)
