/** @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 *   Copyright (C) 2014,2015 Ryan J. F. White
 *
 *   The JavaScript code in this page is free software: you can
 *   redistribute it and/or modify it under the terms of the GNU
 *   General Public License (GNU GPL) as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option)
 *   any later version.  The code is distributed WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 *   As additional permission under GNU GPL version 3 section 7, you
 *   may distribute non-source (e.g., minimized or compacted) forms of
 *   that code without the copy of the GNU GPL normally required by
 *   section 4, provided you include this license notice and a URL
 *   through which recipients can access the Corresponding Source.
 *
 *  @licend  The above is the entire license notice
 *  for the JavaScript code in this page.
 */
function page_init(){
$(".js-show-more").click(function(){
    var cards_now = $(".i-project-card").length;
    var cards_required = cards_now + 12;
    var url = $(this).attr("href")+"&per_page="+cards_required;
    $(".js-show-more").html("Loading...");
    $.ajax({
        type:"GET",
        beforeSend: function (request)
        {
            request.setRequestHeader("X-CSRF-Token",$("[name=authenticity_token]").first ().val())
        },
        url: url,
        data: null,
        success: function(msg) {
            $("body").empty();
            $("body").append(msg);
            page_init();
        },
        error: function (jqXHR,textStatus,errorThrown){
            alert("Error(explore)->>>:"+errorThrown);
        }
    });


    return false;//so we don't resubmit the page
});
}
page_init();
