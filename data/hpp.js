/** @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 *   Copyright (C) 2014,2015 Ryan J. F. White
 *
 *   The JavaScript code in this page is free software: you can
 *   redistribute it and/or modify it under the terms of the GNU
 *   General Public License (GNU GPL) as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option)
 *   any later version.  The code is distributed WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 *   As additional permission under GNU GPL version 3 section 7, you
 *   may distribute non-source (e.g., minimized or compacted) forms of
 *   that code without the copy of the GNU GPL normally required by
 *   section 4, provided you include this license notice and a URL
 *   through which recipients can access the Corresponding Source.
 *
 *  @licend  The above is the entire license notice
 *  for the JavaScript code in this page.
 */

//this one tries to replicate the process done without the LibreJS plugin
//Next step - turning the form submit into an ajax post
$(function(){
    $("body").css("font-family","sans-serif");
    $("#container").append($(".basetable").first());//even better
    //$("#pageform").attr("action","completeCard.shtml");
    $("#pageform").attr("action","foo.shtml");//since we're using ajax, this becomes redundant
//    $("[name=pay]").first().value = "Pay now";
    $("[name=pay]").first().get(0).value = "Pay Now";
    //form fields aren't obvious - and some campaigns work through another system...
    $("[name=pay]").click(function(event){
        $("[name=pay]").first().get(0).value = "Working...";
        event.preventDefault();
        var form_path = "https://www.payments.indiegogo.com/hpp/completeCard.shtml";
        var form = $("#pledge_form");
        var form_data = {
            "utf8":$("#pledge_form").find("[name=utf8]").first().val()
            ,"authenticity_token":$("#pledge_form").find("[name=authenticity_token]").first().val()
            ,"current_page":$("#pledge_form").find("[name=current_page]").first().val()
            ,"pledge[pledge_type]":$("#pledge_form").find("[name=pledge\\[pledge_type\\]]").first().val()
            ,"displayGroup":"card"
            ,"card.cardHolderName":$("#card\\.cardHolderName").val()
            ,"card.cardNumber":$("#card\\.cardNumber").val()
            ,"card.cvcCode":$("#card\\.cvcCode").val()
            ,"card.expiryMonth":$("#card\\.expiryMonth").val()
            ,"card.expiryYear":$("#card\\.expiryYear").val()
            ,"card.billingAddress.country":$("#card\\.billingAddress\\.country").val()
            ,"card.billingAddress.houseNumberOrName":$("#card\\.billingAddress\\.houseNumberOrName").val()
            ,"card.billingAddress.street":$("#card\\.billingAddress\\.street").val()
            ,"card.billingAddress.city":$("#card\\.billingAddress\\.city").val()
            ,"card.billingAddress.postalCode":$("#card\\.billingAddress\\.postalCode").val()
            ,"sig":$("[name=sig]").val()
            ,"merchantReference":$("[name=merchantReference]").val()
            ,"brandCode":$("[name=brandCode]").val()
            ,"paymentAmount":$("[name=paymentAmount]").val()
            ,"currencyCode":$("[name=currencyCode]").val()
            ,"shipBeforeDate":$("[name=shipBeforeDate]").val()
            ,"skinCode":$("[name=skinCode]").val()
            ,"merchantAccount":$("[name=merchantAccount]").val()
            ,"shopperLocale":$("[name=shopperLocale]").val()
            ,"stage":$("[name=stage]").val()
            ,"sessionId":$("[name=sessionId]").val()
            ,"orderData":$("[name=orderData]").val()
            ,"sessionValidity":$("[name=sessionValidity]").val()
            ,"shopperEmail":$("[name=shopperEmail]").val()
            ,"shopperReference":$("[name=]").val()
            ,"resURL":$("[name=resURL]").val()
            ,"originalSession":$("[name=originalSession]").val()
            ,"referrerURL":$("[name=referrerURL]").val()
            ,"usingFrame":$("#usingFrame").val()
            ,"usingPopUp":$("#usingPopUp").val()

        }

console.log(form_data);
        $.ajax({
            type:"POST",
            beforeSend: function (request)
            {
                request.setRequestHeader("X-CSRF-TOKEN", form_data["authenticity_token"])
            },
            url: form_path,
            data: form_data,
            success: function(msg) {
                console.log("result of form submission:");
                console.log(msg);
                //alert(msg);
                //$('body').append(msg);
                //$("#adyen_info").submit();

                res = '{}';
                try {
                    res = $.parseJSON(msg);
                    alert("returned JSON");
                } catch (e) {
                    // not json
                    //alert("Returned SOMETHING ELSE");
                    //alert(msg);
                    $("body").empty();
                    $("body").append(msg);
                }
            },
            error: function(jqXHR,textStatus,errorThrown){
console.log("ERROR:::");
console.log(jqXHR);
console.log(textStatus);
console.log(errorThrown);
},
            complete: function(jqXHR,textStatus){
                console.log("COMPLETED");
                console.log(jqXHR);
                console.log(textStatus);
if(jqXHR.readyState == 0 && jqXHR.responseText== "" && jqXHR.status == 0){
alert("Cool - it looks like things happened. Check your mail for confirmation");
}
            }
        });





    });




});


alert("If you're here, your chances of being able to contribute are high.\nComplete all the fields in the form.");
