/** @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 *   Copyright (C) 2014,2015 Ryan J. F. White
 *
 *   The JavaScript code in this page is free software: you can
 *   redistribute it and/or modify it under the terms of the GNU
 *   General Public License (GNU GPL) as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option)
 *   any later version.  The code is distributed WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 *   As additional permission under GNU GPL version 3 section 7, you
 *   may distribute non-source (e.g., minimized or compacted) forms of
 *   that code without the copy of the GNU GPL normally required by
 *   section 4, provided you include this license notice and a URL
 *   through which recipients can access the Corresponding Source.
 *
 *  @licend  The above is the entire license notice
 *  for the JavaScript code in this page.
 */
$(".i-home-featured-carousel").hide();
//$(".i-mobile-fullwidth").hide();
$(".js-homepage-video").hide();
$(".i-header-links.i-header-account-links").hide();

//2015-07-02 - explore page does ajax request now, so hide the search section for a bit
//$("div.i-header-links.i-header-menu-links").find("a").first().before("<div style='display:inline;'><input type='text' id='filter_title'><button id='explore-button'><span style='font-weight:bold;'>SEARCH</span></button></div><a></a>");

$("#explore-button").click(function(){
    location.href="/explore/?filter_title="+$("#filter_title").val();
});

//hide the life stuff - we don't support it yet
$(".i-home-ringfence").hide();
