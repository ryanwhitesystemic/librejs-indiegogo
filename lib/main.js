/** @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 *   Copyright (C) 2014 Ryan J. F. White
 *
 *   The JavaScript code in this page is free software: you can
 *   redistribute it and/or modify it under the terms of the GNU
 *   General Public License (GNU GPL) as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option)
 *   any later version.  The code is distributed WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 *   As additional permission under GNU GPL version 3 section 7, you
 *   may distribute non-source (e.g., minimized or compacted) forms of
 *   that code without the copy of the GNU GPL normally required by
 *   section 4, provided you include this license notice and a URL
 *   through which recipients can access the Corresponding Source.
 *
 *  @licend  The above is the entire license notice
 *  for the JavaScript code in this page.
 */


//detect when we're on an igg page and change the login link to point to the login page
//LATEST STATUS - we're not worrying about tweaking the login button at this point.(May 2014 updates)
//contributions can be done without logging in.

var data = require("sdk/self").data;
var pageMod = require("sdk/page-mod");

//These PageMod methods allow us to include scripts to modify and manipulate pages on loading.


//generic include
pageMod.PageMod({
    include: "*.indiegogo.com",
    contentScriptWhen: "ready",
    contentScriptFile: [data.url("jquery-1.11.3.js"),
data.url ("base.js")]
});


//Project page
pageMod.PageMod({
    include: /.*indiegogo\.com.*projects.*/,
    contentScriptFile: [data.url("jquery-1.11.3.js"),
data.url ("projects.js")],
contentScriptWhen: "ready"
});


//The contributions page, in which the user selects their perk
pageMod.PageMod({
    include: /.*indiegogo\.com.*contributions.*/,
    contentScriptFile: [data.url("jquery-1.11.3.js"),
                        data.url("LibreStripe.js"),
                        data.url ("contributions.js")]
});

//part of the payment process - the page after perk selection
pageMod.PageMod({
    include: /.*indiegogo\.com.*adyen*/,
    contentScriptFile: [data.url("jquery-1.11.3.js"),
                        data.url ("adyen.js")]
});

//the page after adyen - the credit card processing page
pageMod.PageMod({
    include: /.*pay\.shtml*/,
    contentScriptFile: [data.url("jquery-1.11.3.js"),
                        data.url ("hpp.js")]
});

//explore - search results
pageMod.PageMod({
    include: /.*indiegogo\.com.*explore.*/,
    contentScriptWhen: "ready",
    contentScriptFile: [data.url("jquery-1.11.3.js"),
                        data.url ("explore.js")]
});
